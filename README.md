# Help Rio Grande do Sul

![Help Rio Grande do Sul](https://www.cnnbrasil.com.br/wp-content/uploads/sites/12/2023/09/FUP20230904227.jpg?w=732&h=412&crop=1)

On April 27, the state of Rio Grande do Sul was devastated by unprecedented thunderstorms. These severe weather events led to catastrophic floods that impacted over 85% of the state. The widespread destruction and loss have been profound. This project aims to raise awareness and solicit donations to support the recovery efforts for those affected by this disaster.

## Table of Contents

- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)
- [Support and Donations](#support-and-donations)

## Introduction

This project is intended to help fetch donations to support families affected by the thunderstorms in Rio Grande do Sul. Every bit of help is welcome.

## Installation

To get started with this project, follow these steps:

1. Clone the repository:
  ```bash git clone https://gitlab.com/AlvaroPereira/help-rio-grande-do-sul```

2. Navigate to the project directory:
  ```bash cd help-rio-grande-do-sul```

3. Install the dependencies:
  ```bash npm install```

## Usage

To run the project locally:

```bash npm start```

## Contributing
Please follow these guidelines for contributing to the project:

- The master branch is closed to direct pushes. The only way to publish on master is through a merge.
- Do not use prefixes on branch names, like "feat", "fix", etc.
- Do not write commits with more than 40 characters.
- All merge requests must contain a reviewer and a detailed description. Merge requests without complete and clear descriptions of the changes will be refused instantly.

## License
This project is licensed under the MIT License. See the LICENSE file for more details.

Support and Donations
If you want to support the cause or the project, feel free to contribute. If you want to help the victims of the disaster, you can visit this [link](https://gofund.me/446d249d).

## Useful links
[Defesa Civil do estado do Rio Grande do Sul](https://defesacivil.rs.gov.br/avisos-e-alertas)