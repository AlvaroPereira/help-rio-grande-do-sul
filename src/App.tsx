import { BrowserRouter, Routes, Route } from "react-router-dom";
import Homepage from "./pages/homepage";
import "./index.css";

const app = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Homepage/>} />
          <Route path="*" element={<>Not found</>} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default app;