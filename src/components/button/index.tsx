import styled from "styled-components";

const DonateButton = styled.a`
    display: inline-block;
    margin-top: 20px;
    padding: 15px 30px;
    background: #e74c3c;
    color: white;
    text-decoration: none;
    border-radius: 5px;
    font-size: 1.2em;
    transition: background 0.3s ease;
    
    &:hover {
        background: #c0392b;
    }
`;

export { DonateButton };