import styled from "styled-components";

const FullScreenContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
    background: #000;
    padding: 20px;
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
`;

const Container = styled.div`
    background: #333;
    padding: 30px;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(255, 255, 255, 0.1);
    max-width: 700px;
    text-align: center;
    color: #fff;
    width: 100%;
`;

export { FullScreenContainer, Container }