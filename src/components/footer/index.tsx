import styled from "styled-components";

const Footer = styled.div`
    margin-top: 30px;
    font-size: 0.9em;
    color: #bbb;
`;

export { Footer };