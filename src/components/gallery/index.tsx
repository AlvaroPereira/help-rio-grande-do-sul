import React, { useState, useEffect } from 'react';
import {Image, ImageGallery, LightboxImage, Overlay} from "./styles.tsx";

interface LightboxProps {
    images: string[];
}

const Lightbox: React.FC<LightboxProps> = ({ images }) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [currentImage, setCurrentImage] = useState<string | null>(null);

    const openLightbox = (image: string) => {
        setCurrentImage(image);
        setIsOpen(true);
    };

    const closeLightbox = () => {
        setIsOpen(false);
        setCurrentImage(null);
    };

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.key === 'Escape') {
            closeLightbox();
        }
    };

    useEffect(() => {
        if (isOpen) {
            document.addEventListener('keydown', handleKeyDown);
        } else {
            document.removeEventListener('keydown', handleKeyDown);
        }

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [isOpen]);

    return (
        <>
            <ImageGallery>
                {images.map((image, index) => (
                    <Image key={index} src={image} alt={`Gallery image ${index + 1}`}
                         onClick={() => openLightbox(image)}/>
                ))}
            </ImageGallery>

            <Overlay isOpen={isOpen} onClick={closeLightbox}>
            {currentImage && <LightboxImage src={currentImage} alt="Current view" />}
            </Overlay>
        </>
    );
};

export default Lightbox;