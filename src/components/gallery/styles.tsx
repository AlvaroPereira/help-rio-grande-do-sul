import styled from "styled-components";


interface OverlayProps {
    isOpen: boolean;
}

const ImageGallery = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 10px;
    margin-top: 20px;
`;

const Image = styled.img`
    width: 100px;
    height: 100px;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(255, 255, 255, 0.2);
    cursor: pointer;
`;

const Overlay = styled.div<OverlayProps>`
    display: ${({ isOpen }) => (isOpen ? 'flex' : 'none')};
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.8);
    justify-content: center;
    align-items: center;
    z-index: 1000;
`;

const LightboxImage = styled.img`
    max-width: 90%;
    max-height: 90%;
    border-radius: 10px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);
`;

export { ImageGallery, Image, Overlay, LightboxImage };