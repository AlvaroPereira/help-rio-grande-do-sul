import styled from "styled-components";

const Title = styled.h1`
    color: #fff;
    font-size: 2.5em;
    margin-bottom: 20px;
`;

const Subtitle = styled.h2`
    color: #ccc;
    font-size: 1.5em;
    margin-bottom: 15px;
`;

const Paragraph = styled.p`
    line-height: 1.6;
    font-size: 1.1em;
    margin-bottom: 20px;
`;

export { Title, Subtitle, Paragraph };