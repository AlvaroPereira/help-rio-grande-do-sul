import { useEffect, useState } from "react";
import styled from "styled-components";
import { Container, FullScreenContainer } from "../../components/container";
import { Paragraph, Subtitle, Title } from "../../components/typography";
import Lightbox from "../../components/gallery";
import { DonateButton } from "../../components/button";
import { Footer } from "../../components/footer";

const MainSection = styled.div`
    background: #444;
    padding: 20px;
    border-radius: 8px;
    margin-top: 20px;
    box-shadow: 0 2px 4px rgba(255, 255, 255, 0.1);
`;

const Heart = styled.span`
    color: #e74c3c;
`;

const Homepage = () => {
    const [contributors, setContributors] = useState<Array<string>>([]);

    const images = [
        "image-one.jpg?url",
        "image-two.webp?url"
    ]

    useEffect(() => {
        const getContributors = async () => {
            const request = await fetch("https://gitlab.com/api/v4/projects/57911061/members", { method: "GET" });
            const data = await request.json();

            const peoples = data.map((item: { name: string }) => item.name);
            setContributors(peoples);
        };

        getContributors();
    }, []);

    const renderContributors = () => {
        const contributorsCount = contributors.length;

        if (contributorsCount === 1) {
            return contributors[0];
        } else if (contributorsCount === 2) {
            return contributors.join(" and ");
        } else if (contributorsCount <= 5) {
            return contributors.join(", ");
        } else {
            const firstFive = contributors.slice(0, 5).join(", ");
            const othersCount = contributorsCount - 5;
            return (
                <>
                    {firstFive} and{" "}
                    <a href="https://gitlab.com/AlvaroPereira/help-rio-grande-do-sul/-/project_members">
                        {othersCount} others
                    </a>
                </>
            );
        }
    };

    return (
        <>
            <FullScreenContainer>
                <Container>
                    <Title>Help Rio Grande do Sul</Title>
                    <Paragraph>
                        In April 2024, the state of Rio Grande do Sul was devastated by unprecedented thunderstorms, resulting in catastrophic floods. This disaster has affected 85% of the state, causing widespread destruction and loss.
                    </Paragraph>
                    <Subtitle>Impact of the Disaster</Subtitle>
                    <MainSection>
                        <Paragraph>
                            - Thousands of families have been displaced, losing their homes and possessions.<br />
                            - Critical infrastructure, including roads, bridges, and schools, has been severely damaged.<br />
                            - Numerous businesses have been destroyed, leaving many without jobs and income.<br />
                            - The death toll is rising, with many more still missing.
                        </Paragraph>
                    </MainSection>
                    <Subtitle>Your Help is Needed</Subtitle>
                    <Paragraph>
                        We urgently need your support to provide relief and assist in the recovery efforts. Your generous donation can help supply essential items such as food, water, clothing, and medical aid to those in dire need.
                    </Paragraph>
                    <Paragraph>
                        Together, we can make a significant difference in the lives of those affected. Every contribution, no matter how small, brings hope and helps rebuild communities.
                    </Paragraph>
                    <DonateButton href="https://gofund.me/446d249d">Donate Now</DonateButton>
                    <Lightbox images={images} />
                    <Footer>
                        <p>Developed with <Heart>❤</Heart> by {renderContributors()}</p>
                    </Footer>
                </Container>
            </FullScreenContainer>
        </>
    );
};

export default Homepage;